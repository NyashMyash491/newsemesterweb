<?php namespace App\Models;
use CodeIgniter\Model;
class ServiceModel extends Model
{
    protected $table = 'sessions'; //таблица, связанная с моделью
    protected $allowedFields = ['Client_ID', 'Beautician_ID', 'Begin_date', 'End_date']; 
    public function getService($id = null)
    {
        if (!isset($id)) {
            return $this->findAll();
        }
        return $this->where(['Session_ID' => $id])->first();
    }

//    public function getClientValue($id)
//    {
//        return '1';
//        $query = $this->db->get_where('sessions', array('id'=>$id),1);
//        return $query->result();
//    $this->db->('c.client_name');
//    $this->db->from('sessions s');
//    $this->db->join('clients c','s.client_id = c.client_id','inner');
//    $this->db->where('session_id', $id);
//    $query = $this->db->get();
//    if($query->num_rows() > 0)
//        return $query->result();
//    }
//    public function getBeuticianValue($id)
//    {
//    $this->db->select('b.beautician_name');
//    $this->db->from('sessions s');
//    $this->db->join('beauticians b','s.client_id = b.beautician_id','inner');
//    $this->db->where('session_id', $id);
//    $query = $this->db->get();
//    if($query->num_rows() > 0)
//        return $data->result();
//    }
}
?>

