<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="container main">
    <?php use CodeIgniter\I18n\Time; ?>
    <?php if (!empty($service)) : ?>
            <div class="card mb-3" style="max-width: 540px;">
                <div class="row">
                    <div class="col-md-4 d-flex align-items-center">
                            <img height="150" src="https://sankt-peterburg.spravka.ru/uploads/0/0/P3/MT1_3ErwTzKcX2yR9PPf3A8Yq6Madi/original_5832b96b6b0ff1332e8b4571_5832ba0f99f95.jpg" class="card-img" style="margin-left:15px"; alt="<?= esc($service['Session_ID']); ?>">
                    </div>
                    <div class="col-md-8">
                    <div class="card-body">
                        <h5 class="card-title">Косметолог: </h5>
                        <p class="card-title"><?= esc($service['Beautician_ID']); ?></p>
                        <h5 class="card-title">Клиент: </h5>
                        <p class="card-title"><?= esc($service['Client_ID']); ?></p>
                    </div>
                                 <div><a href="<?= base_url()?>/ServiceController/delete/<?= esc($service['Session_ID']); ?>" class="btn btn-danger" style="margin-bottom:15px;">Удалить</a> 
                                    <a href="<?= base_url()?>/ServiceController/edit/<?= esc($service['Session_ID']); ?>" class="btn btn-primary" style="margin-bottom:15px;">Изменить</a>
                                 </div>  
                        </div>
                    </div>
                </div>
            </div>
    <?php else : ?>
        <p>Услуга не найдена</p>
    <?php endif ?>
</div>
<?= $this->endSection() ?>
