<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>

<div class="container" style="max-width: 540px;">

    <?= form_open_multipart('ServiceController/update'); ?>
    <input type="hidden" name="Session_ID" value="<?= $service["Session_ID"] ?>">

      <div class="form-group">
        <label for="Begin_date">Дата записи на услугу</label>
        <input type="date" class="form-control <?= ($validation->hasError('Begin_date')) ? 'is-invalid' : ''; ?>" name="Begin_date"
               value="<?= $service["Begin_date"] ?>">
        <div class="invalid-feedback">
            <?= $validation->getError('Begin_date') ?>
        </div>
       </div>

    <div class="form-group">
        <label for="End_date">Дата оказания услуги:</label>
        <input type="date" class="form-control <?= ($validation->hasError('End_date')) ? 'is-invalid' : ''; ?>" name="End_date"
               value="<?= $service["End_date"] ?>">
        <div class="invalid-feedback">
            <?= $validation->getError('End_date') ?>
        </div>

  <div class="form-group">
        <label for="Client_ID">Имя клиента</label>
        <input type="number" class="form-control <?= ($validation->hasError('Client_ID')) ? 'is-invalid' : ''; ?>" name="Client_ID"
               value="<?= $service["Client_ID"] ?>">
        <div class="invalid-feedback">
            <?= $validation->getError('Client_ID') ?>
        </div>

    <div class="form-group">
        <label for="Beautician_ID">Имя косметолога</label>
        <input type="number" class="form-control <?= ($validation->hasError('Beautician_ID')) ? 'is-invalid' : ''; ?>" name="Beautician_ID"
               value="<?= $service["Beautician_ID"]; ?>">
        <div class="invalid-feedback">
            <?= $validation->getError('Beautician_ID') ?>
        </div>

    </div>

    </div>

    <div class="form-group">
    <button type="submit" class="btn btn-primary" name="submit">Сохранить</button>
    </div>
    </form>
    </div>
<?= $this->endSection() ?>
