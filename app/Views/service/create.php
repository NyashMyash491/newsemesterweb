<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="container" style="max-width: 540px;">

    <?= form_open_multipart('ServiceController/store'); ?>

    <label for="Begin_date">Дата оказания услуги</label>
    <input type="date" class="form-control <?= ($validation->hasError('Begin_date')) ? 'is-invalid' : ''; ?>" name="Begin_date"
           value="<?= old('Begin_date'); ?>">
    <div class="invalid-feedback">
        <?= $validation->getError('Begin_date') ?>
    </div>

    <label for="End_date">Дата оказания услуги</label>
    <input type="date" class="form-control <?= ($validation->hasError('End_date')) ? 'is-invalid' : ''; ?>" name="End_date"
           value="<?= old('End_date'); ?>">
    <div class="invalid-feedback">
        <?= $validation->getError('End_date') ?>
    </div>

    <label for="Client_ID">Имя клиента</label>
    <input type="text" class="form-control <?= ($validation->hasError('Client_ID')) ? 'is-invalid' : ''; ?>" name="Client_ID"
           value="<?= old('Client_ID'); ?>">
    <div class="invalid-feedback">
        <?= $validation->getError('Client_ID') ?>
    </div>

    <div class="form-group">
        <label for="Beautician_ID">Имя косметолога</label>
        <input type="text" class="form-control <?= ($validation->hasError('Beautician_ID')) ? 'is-invalid' : ''; ?>" name="Beautician_ID"
               value="<?= old('Beautician_ID'); ?>">
        <div class="invalid-feedback">
            <?= $validation->getError('Beautician_ID') ?>
        </div>

    </div>

    </div>

    <div class="form-group">
        <button style="margin-top:20px;" type="submit" class="btn btn-primary" name="submit">Создать сеанс</button>
    </div>
    </form>


</div>
<?= $this->endSection() ?>
