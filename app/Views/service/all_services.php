<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="container main">
<h2>Все услуги</h2>

<?php if (!empty($service) && is_array($service)) : ?>

    <?php foreach ($service as $item): ?>

        <div class="card mb-3" style="max-width: 540px;">
            <div class="row">
                <div class="col-md-4 d-flex align-items-center">
                        <img height="150" src="https://дистант-колледж.рф/pluginfile.php/11854/course/overviewfiles/47361-O5A25N-768x599.jpg" class="card-img" alt="<?= esc($item['Session_ID']); ?>">
                </div>
                <div class="col-md-8">
                    <div class="card-body">
                        <h5 class="card-title">Дата записи на услугу: </h5>
                        <p class="card-title"><?= esc($item['Begin_date']); ?></p>
                        <h5 class="card-title">Дата записи на услугу: </h5>
                        <p class="card-title"><?= esc($item['End_date']); ?></p>
                        <a href="<?= base_url()?>/ServiceController/view/<?= esc($item['Session_ID']); ?>" class="btn btn-primary">Просмотреть</a>
                    </div>
                </div>
            </div>
        </div>

    <?php endforeach; ?>

<?php else : ?>

    <p>Невозможно найти услуги</p>

<?php endif ?>
</div>
<?= $this->endSection() ?>
