<?php namespace App\Controllers;

use App\Models\ServiceModel;


class ServiceController extends BaseController
{
    public function index() //Обображение всех записей
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');                
        }
                
        $model = new ServiceModel();
        $data['service'] = $model->getService();
        echo view('service/all_services', $this->withIon($data));
    }

    public function view($id = null) //отображение одной записи
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');                
        }
          
        $model = new ServiceModel();
        $data['service'] = $model->getService($id);
        echo view('service/view', $this->withIon($data));
    }

    public function create()
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        helper(['form']);
        $data ['validation'] = \Config\Services::validation();
        echo view('service/create', $this->withIon($data));
    }

    public function store()
    {
        helper(['form','url']);

        if ($this->request->getMethod() === 'post' && $this->validate([
            'Begin_date'  => 'required',
            'End_date'  => 'required',
            'Client_ID'  => 'required',
            'Beautician_ID'  => 'required',
        ]))
            {
                $model = new ServiceModel();
                $model->save([
                'Begin_date' => $this->request->getPost('Begin_date'),
                'End_date' => $this->request->getPost('End_date'),
                'Client_ID' => $this->request->getPost('Client_ID'),
                'Beautician_ID' => $this->request->getPost('Beautician_ID'),
            ]);
            //session()->setFlashdata('message', 'This Is a Message');
            return redirect()->to('/all_services');
            }
        else
            {
                return redirect()->to('/ServiceController/create')->withInput();
            }
    }   

    public function edit($id)
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new ServiceModel();

        helper(['form']);
        $data ['service'] = $model->getService($id);
        $data ['validation'] = \Config\Services::validation();
        echo view('service/edit', $this->withIon($data));

    }

    public function update()
        {
            helper(['form','url']);
            echo '/ServiceController/edit/'.$this->request->getPost('sessions.Session_ID');
            if ($this->request->getMethod() === 'post' && $this->validate([
                    'Session_ID'  => 'required',
                    'Begin_date'  => 'required',
                    'End_date'  => 'required',
                    'Client_ID'  => 'required',
                    'Beautician_ID'  => 'required',
                ]))
            {
                $model = new ServiceModel();
                $data = [
                    'Begin_date' => $this->request->getPost('Begin_date'),
                    'End_date' => $this->request->getPost('End_date'),
                    'Client_ID' => $this->request->getPost('Client_ID'),
                    'Beautician_ID' => $this->request->getPost('Beautician_ID'),
                ];
                $model->update($this->request->getPost('sessions.Session_ID'), $data);
                //session()->setFlashdata('message', 'This Is a Message');

                return redirect()->to('/all_services');
            }
            else
            {
               return redirect()->to('/ServiceController/edit/'.$this->request->getPost('sessions.Session_ID'))->withInput();
            }
        }
   
    public function delete($id)
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new ServiceModel();
        $model->delete($id);
        return redirect()->to('/all_services');
    }
}

