<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class MyMigration extends Migration
{
	public function up()
	{

    if (!$this->db->tableexists('beauticians'))
    {
        // Setup Keys
        $this->forge->addkey('beautician_id', TRUE);

        $this->forge->addfield(array(
            'beautician_id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
            'beautician_name' => array('type' => 'VARCHAR', 'constraint' => '20', 'null' => TRUE)
        ));
        // create table
        $this->forge->createtable('beauticians', TRUE);
    }

    // clients
    if (!$this->db->tableexists('clients'))
    {
        // Setup Keys
        $this->forge->addkey('client_id', TRUE);

        $this->forge->addfield(array(
            'client_id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
            'client_name' => array('type' => 'VARCHAR', 'constraint' => '20', 'null' => TRUE)

        ));
        // create table
        $this->forge->createtable('clients', TRUE);
    }

    if (!$this->db->tableexists('services'))
    {
        // services
        $this->forge->addkey('service_id', TRUE);

        $this->forge->addfield(array(
            'service_id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
            'service_name' => array('type' => 'VARCHAR', 'constraint' => '20', 'null' => TRUE),
            'service_price' => array('type' => 'INT', 'null' => TRUE)
        ));
        // create table
        $this->forge->createtable('services', TRUE);
    }

        if (!$this->db->tableexists('sessions'))
        {
            // Setup Keys
            $this->forge->addkey('session_id', TRUE);

            $this->forge->addfield(array(
                'session_id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'client_id' => array('type' => 'INT', 'null' => TRUE),
                'beautician_id' => array('type' => 'INT', 'null' => TRUE),
                'begin_date' => array('type' => 'DATE', 'null' => TRUE),
                'end_date' => array('type' => 'DATE', 'null' => TRUE)
            ));
            $this->forge->addForeignKey('client_id','clients','client_id','RESTRICT','RESRICT');
            $this->forge->addForeignKey('beautician_id','beauticians','beautician_id','RESTRICT','RESRICT');
            // create table
            $this->forge->createtable('sessions', TRUE);
        }

        // service_provided
        if (!$this->db->tableexists('service_provided'))
        {
            $this->forge->addfield(array(
                'session_id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE),
                'service_id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE)
            ));
            $this->forge->addForeignKey('session_id','sessions','session_id','RESTRICT','RESRICT');
            $this->forge->addForeignKey('service_id','services','service_id','RESTRICT','RESRICT');
            // create table
            $this->forge->createtable('service_provided', TRUE);

        }
}

	//--------------------------------------------------------------------

	public function down()
    {
    $this->forge->droptable('beauticians');
    $this->forge->droptable('clients');
    $this->forge->droptable('services');
    $this->forge->droptable('sessions');
    $this->forge->droptable('service_provided');
    }
}

