<?php namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class MySeeder extends Seeder
{
    public function run()
    {
//        $data = [
//            'beautician_name' => 'Ivanova',
//        ];
//        $this->db->table('beauticians')->insert($data);
//
//        $data = [
//            'beautician_name' => 'Smirnova'
//        ];
//        $this->db->table('beauticians')->insert($data);
//
//        $data = [
//            'client_name'=> 'Ksusha'
//        ];
//        $this->db->table('clients')->insert($data);
//
//        $data = [
//            'client_name'=> 'Anna'
//        ];
//        $this->db->table('clients')->insert($data);
//
//        $data = [
//            'service_name'=> 'Nail polishing',
//            'service_price'=>'1500'
//        ];
//        $this->db->table('services')->insert($data);
//
//        $data = [
//            'service_name'=> 'Haircut',
//            'service_price'=>'2000'
//        ];
//        $this->db->table('services')->insert($data);

        $data = [
            'client_id'=> '1',
            'beautician_id' => '1',
            'begin_date' => '2021-01-30',
            'end_date' => '2021-01-30'
        ];
        $this->db->table('sessions')->insert($data);

        $data = [
            'client_id'=> '2',
            'beautician_id' => '2',
            'begin_date' => '2022-01-30',
            'end_date' => '2022-01-30'
        ];
        $this->db->table('sessions')->insert($data);

        $data = [
            'session_id'=> '1',
            'service_id' => '2'
        ];
        $this->db->table('service_provided')->insert($data);

        $data = [
            'session_id'=> '2',
            'service_id' => '1'
        ];

        $this->db->table('service_provided')->insert($data);

    }
}