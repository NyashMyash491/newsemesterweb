-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Хост: localhost:3306
-- Время создания: Янв 04 2021 г., 03:23
-- Версия сервера: 8.0.22-0ubuntu0.20.04.3
-- Версия PHP: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `60501m_gam`
--

-- --------------------------------------------------------

--
-- Структура таблицы `beauticians`
--

CREATE TABLE `beauticians` (
  `Beautician_ID` int UNSIGNED NOT NULL,
  `Beautician_name` varchar(20) DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=1638 DEFAULT CHARSET=cp1251;

--
-- Дамп данных таблицы `beauticians`
--

INSERT INTO `beauticians` (`Beautician_ID`, `Beautician_name`) VALUES
(1, 'Иванова'),
(2, 'Петрова'),
(3, 'Сидорова'),
(4, 'Смирнова'),
(5, 'Кузнецова'),
(6, 'Васильева'),
(7, 'Федорова'),
(8, 'Михайлова'),
(9, 'Степанова'),
(10, 'Захарова');

-- --------------------------------------------------------

--
-- Структура таблицы `blocks`
--

CREATE TABLE `blocks` (
  `ID` int NOT NULL,
  `Date` date NOT NULL,
  `Beautician_name` varchar(255) NOT NULL,
  `Client_name` varchar(255) NOT NULL,
  `Service_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `blocks`
--

INSERT INTO `blocks` (`ID`, `Date`, `Beautician_name`, `Client_name`, `Service_name`) VALUES
(3, '9494-02-20', '222222222222', '11111111111', 'wevwewev'),
(7, '2020-10-10', 'Artur', 'Artur', 'Artur');

-- --------------------------------------------------------

--
-- Структура таблицы `clients`
--

CREATE TABLE `clients` (
  `Client_ID` int UNSIGNED NOT NULL,
  `Client_name` varchar(20) DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=1638 DEFAULT CHARSET=cp1251;

--
-- Дамп данных таблицы `clients`
--

INSERT INTO `clients` (`Client_ID`, `Client_name`) VALUES
(1, 'Ксения'),
(2, 'Анна'),
(3, 'Екатерина'),
(4, 'Дарья'),
(5, 'Александра'),
(6, 'Валентина'),
(7, 'Галина'),
(8, 'Любовь\r\n'),
(9, 'Наталья'),
(10, 'Елена');

-- --------------------------------------------------------

--
-- Структура таблицы `groups`
--

CREATE TABLE `groups` (
  `id` mediumint UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(2, 'members', 'General User');

-- --------------------------------------------------------

--
-- Структура таблицы `login_attempts`
--

CREATE TABLE `login_attempts` (
  `id` int UNSIGNED NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `services`
--

CREATE TABLE `services` (
  `Service_ID` int UNSIGNED NOT NULL,
  `Service_name` varchar(20) DEFAULT NULL,
  `Service_price` int DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=1638 DEFAULT CHARSET=cp1251;

--
-- Дамп данных таблицы `services`
--

INSERT INTO `services` (`Service_ID`, `Service_name`, `Service_price`) VALUES
(1, 'Чистка', 1000),
(2, 'Маска1', 3500),
(3, 'Маска2', 3200),
(4, 'Маска3', 3600),
(5, 'Пилинг', 7000),
(6, 'Пластика', 5500),
(7, 'Массаж', 4000),
(8, 'Маникюр', 3000),
(9, 'Маникюр1', 3000),
(10, 'Маникюр2', 3500);

-- --------------------------------------------------------

--
-- Структура таблицы `service_provided`
--

CREATE TABLE `service_provided` (
  `Session_ID` int UNSIGNED DEFAULT NULL,
  `Service_ID` int UNSIGNED DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=1638 DEFAULT CHARSET=cp1251;

--
-- Дамп данных таблицы `service_provided`
--

INSERT INTO `service_provided` (`Session_ID`, `Service_ID`) VALUES
(5, 3),
(9, 7),
(4, 3),
(6, 7),
(7, 1),
(8, 1),
(6, 2),
(3, 4),
(1, 1),
(2, 3),
(5, 4),
(3, 3),
(5, 5),
(5, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `sessions`
--

CREATE TABLE `sessions` (
  `Session_ID` int UNSIGNED NOT NULL,
  `Client_ID` int UNSIGNED DEFAULT NULL,
  `Beautician_ID` int UNSIGNED DEFAULT NULL,
  `Begin_date` datetime DEFAULT NULL,
  `End_date` datetime DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=1260 DEFAULT CHARSET=cp1251;

--
-- Дамп данных таблицы `sessions`
--

INSERT INTO `sessions` (`Session_ID`, `Client_ID`, `Beautician_ID`, `Begin_date`, `End_date`) VALUES
(1, 1, 2, '2020-01-14 15:23:44', '2020-01-14 17:45:55'),
(2, 1, 3, '2020-01-13 15:22:48', '2020-01-13 17:45:39'),
(3, 1, 5, '2020-01-12 15:21:46', '2020-01-12 17:45:37'),
(4, 1, 4, '2020-01-11 15:20:55', '2020-01-11 17:45:35'),
(5, 1, 2, '2020-01-10 15:20:22', '2020-01-10 17:45:31'),
(6, 2, 1, '2020-02-06 17:13:25', '2020-02-06 19:16:26'),
(7, 3, 1, '2020-01-07 20:17:20', '2020-01-07 21:31:15'),
(8, 1, 2, '2020-01-09 15:20:00', '2020-01-09 17:45:31'),
(9, 1, 7, '2020-01-09 15:20:00', '2020-01-09 17:50:31'),
(10, 4, 7, '2020-01-09 18:00:32', '2020-01-09 18:50:31');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int UNSIGNED NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(254) NOT NULL,
  `activation_selector` varchar(255) DEFAULT NULL,
  `activation_code` varchar(255) DEFAULT NULL,
  `forgotten_password_selector` varchar(255) DEFAULT NULL,
  `forgotten_password_code` varchar(255) DEFAULT NULL,
  `forgotten_password_time` int UNSIGNED DEFAULT NULL,
  `remember_selector` varchar(255) DEFAULT NULL,
  `remember_code` varchar(255) DEFAULT NULL,
  `created_on` int UNSIGNED NOT NULL,
  `last_login` int UNSIGNED DEFAULT NULL,
  `active` tinyint UNSIGNED DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `email`, `activation_selector`, `activation_code`, `forgotten_password_selector`, `forgotten_password_code`, `forgotten_password_time`, `remember_selector`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`) VALUES
(1, '127.0.0.1', 'administrator', '$2y$12$PldoNwGbnvG02hD33Q6e4OjvHRNdheCl29zRTV92b2tAoqGPItxSS', 'admin@admin.com', NULL, '', NULL, NULL, NULL, NULL, NULL, 1268889823, 1609712368, 1, 'Admin', 'istrator', 'ADMIN', '0');

-- --------------------------------------------------------

--
-- Структура таблицы `users_groups`
--

CREATE TABLE `users_groups` (
  `id` int UNSIGNED NOT NULL,
  `user_id` int UNSIGNED NOT NULL,
  `group_id` mediumint UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(1, 1, 1),
(2, 1, 2);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `beauticians`
--
ALTER TABLE `beauticians`
  ADD PRIMARY KEY (`Beautician_ID`);

--
-- Индексы таблицы `blocks`
--
ALTER TABLE `blocks`
  ADD PRIMARY KEY (`ID`);

--
-- Индексы таблицы `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`Client_ID`);

--
-- Индексы таблицы `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`Service_ID`);

--
-- Индексы таблицы `service_provided`
--
ALTER TABLE `service_provided`
  ADD KEY `Service_ID` (`Service_ID`),
  ADD KEY `Session_ID` (`Session_ID`);

--
-- Индексы таблицы `sessions`
--
ALTER TABLE `sessions`
  ADD PRIMARY KEY (`Session_ID`),
  ADD KEY `Beautician_ID` (`Beautician_ID`),
  ADD KEY `Client_ID` (`Client_ID`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_email` (`email`),
  ADD UNIQUE KEY `uc_activation_selector` (`activation_selector`),
  ADD UNIQUE KEY `uc_forgotten_password_selector` (`forgotten_password_selector`),
  ADD UNIQUE KEY `uc_remember_selector` (`remember_selector`);

--
-- Индексы таблицы `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  ADD KEY `fk_users_groups_users1_idx` (`user_id`),
  ADD KEY `fk_users_groups_groups1_idx` (`group_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `beauticians`
--
ALTER TABLE `beauticians`
  MODIFY `Beautician_ID` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT для таблицы `blocks`
--
ALTER TABLE `blocks`
  MODIFY `ID` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT для таблицы `clients`
--
ALTER TABLE `clients`
  MODIFY `Client_ID` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT для таблицы `groups`
--
ALTER TABLE `groups`
  MODIFY `id` mediumint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `login_attempts`
--
ALTER TABLE `login_attempts`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `services`
--
ALTER TABLE `services`
  MODIFY `Service_ID` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT для таблицы `sessions`
--
ALTER TABLE `sessions`
  MODIFY `Session_ID` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `service_provided`
--
ALTER TABLE `service_provided`
  ADD CONSTRAINT `service_provided_ibfk_1` FOREIGN KEY (`Session_ID`) REFERENCES `sessions` (`Session_ID`),
  ADD CONSTRAINT `service_provided_ibfk_2` FOREIGN KEY (`Service_ID`) REFERENCES `services` (`Service_ID`);

--
-- Ограничения внешнего ключа таблицы `sessions`
--
ALTER TABLE `sessions`
  ADD CONSTRAINT `sessions_ibfk_1` FOREIGN KEY (`Client_ID`) REFERENCES `clients` (`Client_ID`),
  ADD CONSTRAINT `sessions_ibfk_2` FOREIGN KEY (`Beautician_ID`) REFERENCES `beauticians` (`Beautician_ID`);

--
-- Ограничения внешнего ключа таблицы `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
